// ==UserScript==
// @name        Pardus Hack Helper
// @namespace   http://userscripts.xcom-alliance.info/
// @author      Miche (Orion) / Sparkle (Artemis)
// @version     2.1
// @description Adds the ability to store pilot lists and retrieve them for quick hacking in the Hack screen. It also adjusts the position of the final hack to better accommodate a wide screen.
// @include     http*://*.pardus.at/hack.php*
// @updateURL   http://userscripts.xcom-alliance.info/hack_helper/pardus_hack_helper.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/hack_helper/pardus_hack_helper.user.js
// @icon        http://userscripts.xcom-alliance.info/hack_helper/icon.png
// @grant          GM_setValue
// @grant          GM_getValue
// @grant          GM_log
// ==/UserScript==
