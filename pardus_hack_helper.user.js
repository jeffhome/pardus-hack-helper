// ==UserScript==
// @name        Pardus Hack Helper
// @namespace   http://userscripts.xcom-alliance.info/
// @author      Miche (Orion) / Sparkle (Artemis)
// @version     2.1
// @description Adds the ability to store pilot lists and retrieve them for quick hacking in the Hack screen. It also adjusts the position of the final hack to better accommodate a wide screen.
// @include     http*://*.pardus.at/hack.php*
// @updateURL   http://userscripts.xcom-alliance.info/hack_helper/pardus_hack_helper.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/hack_helper/pardus_hack_helper.user.js
// @icon        http://userscripts.xcom-alliance.info/hack_helper/icon.png
// @grant          GM_setValue
// @grant          GM_getValue
// @grant          GM_log
// ==/UserScript==

// v2.01 - updated to support new grant feature
// v2.1 - released with completed functionality

/*
*******************************************************************************************************************************
							Hacking Helper Section
*******************************************************************************************************************************
*/

function hackingHelperInit() {

	function _addHackHelperCSSToBody() {
		var hackHelperCSS = document.createElement("style");
		hackHelperCSS.setAttribute("type", "text/css");
		var _css = '';
		_css += '#hackHelperNameListDivHeading, #hackHelperNameListDiv2Heading { text-align:left;padding: 2px 0 5px 0;font-weight:bold;font-size:18px;font-family:Lucida Grande; } ';
		_css += '#addToList1Btn, #addToList2Btn, #clearList1Btn, #clearList2Btn, #toggleTopList, #toggleSecondList { -moz-border-radius:5px;padding:2px;margin:0px;color:#fff; position:absolute;top:-30px;}';
		_css += '#toggleTopList, #toggleSecondList { background-color:#d92;color:#000;right:157px;}';
		_css += '#addToList1Btn, #addToList2Btn { background-color:#036;right:70px;}';
		_css += '#clearList1Btn, #clearList2Btn { background-color:#c20;right:0px;}';
		_css += '#hackHelperFirstNode .instructionMessage, #hackHelperSecondNode .instructionMessage { color:#fff; font-style:italic; }';
		_css += '#hackHelperNameListDiv, #hackHelperNameListDiv2 { position:relative;min-height:60px;border-top:1px solid #CCC;border-bottom:1px solid #CCC;width:400px;float:left;background-color:#025;padding:8px 3px; }';
		_css += '#hackHelperNameListDivHeading { border-top:1px solid #999;padding-top:17px; }';
		_css += '#hackHelperFirstNode { padding-top:15px; }';
		_css += '#hackHelperSecondNode { padding-top:10px; }';
		_css += '.nameToHackStyles { float:left;padding:2px 0px 2px 4px;margin:2px 6px 3px 0px;background-color:#000;border:1px solid #999; }';
		_css += '.deleteNameStyles { cursor:pointer;font-weight:bold;color:#b00;line-height:9px;font-size:18px;vertical-align:top; }';
		_css += '#addToList1Btn.locked, #addToList2Btn.locked, #clearList1Btn.locked, #clearList2Btn.locked { background-color:#bbb;color:#999; }';
		_css += '#hackHelperNameListDiv.locked .deleteNameStyles, #hackHelperNameListDiv2.locked .deleteNameStyles { color:#333; } ';
		_css += '#hackHelperNameListDiv.locked .deleteNameStyles:hover, #hackHelperNameListDiv2.locked .deleteNameStyles:hover { text-decoration:none; } ';
		_css += '';
		// style the normal hack screen so that it is in keeping with our look-and-feel
		_css += '#doHackButton { -moz-border-radius:5px;padding:5px 17px;margin:0 0 5px 0;color:#fff;background-color:#052;font-size:15px; }';
		_css += '#doHackButton.disabled { background-color:#bbb;color:#999; }';
		_css += 'button { -moz-border-radius:5px;padding:3px 5px;margin:0px;color:#fff;background-color:#036;width:auto!important;margin-left:7px; }';
		_css += 'input[type="text"] { width:130px!important;-moz-border-radius:5px;padding:4px;margin-bottom:0px!important;margin-top:10px; } ';
		_css += 'form br {display:none;}';
		_css += 'form[name="hacking"] { width:150px;float:left; } ';
		_css += 'form[name="hacking"] select { width:115px!important;-moz-border-radius:5px;padding:3px;margin-top:10px; } ';
		_css += 'form[name="hacking_temp"] select { width:140px!important;-moz-border-radius:5px;padding:3px;margin-top:5px;margin-right:69px; } ';
		_css += '#apcost {float:right;margin-top:15px!important;}';
		_css += '';
		// these styles allow us to see the cloned hack data to the right of the page
		_css += '#hackWrapper {position: absolute; top:30px;right: 30px;}';
		_css += '#hackWrapper>table {margin-bottom:20px;}';
		_css += '';

		hackHelperCSS.innerHTML = _css;
		document.body.appendChild(hackHelperCSS);
	};

	// add some css to prevent lots of inline styles
	_addHackHelperCSSToBody();

	// force us to default to using Freaks every page load
	var useIndex = 3;
	if (document.getElementsByName('hacking')[0].getElementsByTagName('select')[0].options[2].value == 'freak') {
		useIndex = 2;
	}
	document.getElementsByName('hacking')[0].getElementsByTagName('select')[0].selectedIndex=useIndex;

	// get our location to attach new content into
	var btns = document.getElementsByTagName('button');
	var table;
	for (var i=0; i<btns.length; i++) {
		if (btns[i].innerHTML == "Hack") {
			table = btns[i].parentNode.parentNode.parentNode;
		}
	}

	// create and insert a new row for the top hacking list
	var childHeading = document.createElement("div");
	childHeading.setAttribute("id", "hackHelperNameListDivHeading");
	childHeading.innerHTML = "Hacking Helper";
	var child = document.createElement("div");
	child.setAttribute("id", "hackHelperNameListDiv");
	var row = table.insertRow(-1);
	var cell = row.insertCell(-1);
	cell.colSpan = 2;
	cell.id = 'hackHelperFirstNode';
	cell.appendChild(childHeading);
	cell.appendChild(child);
	writeNameList('topList');

	// create and insert a new row for the second hacking list
	var child2Heading = document.createElement("div");
	child2Heading.setAttribute("id", "hackHelperNameListDiv2Heading");
	child2Heading.innerHTML = "Daily Hack List";
	var child2 = document.createElement("div");
	child2.setAttribute("id", "hackHelperNameListDiv2");
	var row = table.insertRow(-1);
	var cell = row.insertCell(-1);
	cell.colSpan = 2;
	cell.setAttribute('id','hackHelperSecondNode');
	cell.appendChild(child2Heading);
	cell.appendChild(child2);
	writeNameList('secondList');
};

function lockButtonsAndList(listPlace) {
	if (listPlace == 'topList') {
		var _toggleBtnID = 'toggleTopList';
		var _addButtonId = 'addToList1Btn';
		var _clearListButtonId = 'clearList1Btn';
	} else
	if (listPlace == 'secondList') {
		var _toggleBtnID = 'toggleSecondList';
		var _addButtonId = 'addToList2Btn';
		var _clearListButtonId = 'clearList2Btn';
	} else {
		// unknown caller - log an error
		GM_log('Error running lockButtonsAndList(). Unable to determine the list to lock');
		return;
	}
	saveLock(listPlace, true);
	var toggleButton = document.getElementById(_toggleBtnID);
		toggleButton.value = 'Unlock list';
	var addButton = document.getElementById(_addButtonId);
		addButton.setAttribute('disabled', true);
		addButton.className = 'locked';
	var clearListButton = document.getElementById(_clearListButtonId);
		clearListButton.setAttribute('disabled', true);
		clearListButton.className = 'locked';

	// lock the table contents by placing a class on a node that wraps them all - this will
	// change the colour and can be tested to determine whether to let the click through
	toggleButton.parentNode.className = 'locked';
	
};

function unlockButtonsAndList(listPlace) {
	if (listPlace == 'topList') {
		var _toggleBtnID = 'toggleTopList';
		var _addButtonId = 'addToList1Btn';
		var _clearListButtonId = 'clearList1Btn';
	} else
	if (listPlace == 'secondList') {
		var _toggleBtnID = 'toggleSecondList';
		var _addButtonId = 'addToList2Btn';
		var _clearListButtonId = 'clearList2Btn';
	} else {
		// unknown caller - log an error
		GM_log('Error running unlockButtonsAndList(). Unable to determine the list to unlock');
		return;
	}
	saveLock(listPlace, false);
	var toggleButton = document.getElementById(_toggleBtnID);
		toggleButton.value = 'Lock list';
	var addButton = document.getElementById(_addButtonId);
		addButton.removeAttribute('disabled');
		addButton.className = '';
	var clearListButton = document.getElementById(_clearListButtonId);
		clearListButton.removeAttribute('disabled');
		clearListButton.className = '';

	// unlock the table contents by removing the class on the node that wraps them all - this will
	// change the colour and can be tested to determine whether to let the click through
	toggleButton.parentNode.className = '';
};

// toggle the lock/unlock feature to prevent "accidental" alteration of the list contents by a mis-click
function toggleLockList(e) {
	var _clickedButton = e.target;
	if (e.target.id == 'toggleTopList') {
		var listPlace = 'topList';
	} else 
	if (e.target.id == 'toggleSecondList') {
		var listPlace = 'secondList';
	} else {
		// unknown caller - log an error
		GM_log('Error running toggleLockList(). Unable to determine the list to toggle');
		return;
	}
	if (e.target.value == 'Lock list') {
		lockButtonsAndList(listPlace);
	} else {
		unlockButtonsAndList(listPlace);
	}
};

// write out the list of pilots to be hacked and related html furniture
function writeNameList(listPlace) {
	var EMPTY_HACKLIST_BOX_INSTRUCTION_MESSAGE_HTML = '<span class="instructionMessage">There are currently no pilots saved in this list.<br><br>Use the &quot;<b>Add a name</b>&quot; button above each list to add the name of the pilot you have typed in order to perform a hack.</span>';
	var EMPTY_HACKLIST_BOX_TOPLIST_EXTRA_INSTRUCTION_MESSAGE_HTML = '<br><br><span class="instructionMessage">You can also use the &quot;<b>Append all to top list</b>&quot; button in the &quot;<b>Alliance Helper</b>&quot; section (top left) to add a large number of pilots in one click.</span>';
	if (listPlace == 'topList') {
		// these differ between the lists
		var _addToListID = 'addToList1Btn';
		var _clearListID = 'clearList1Btn';
		var _wrapperDivID = 'hackHelperNameListDiv';
		var _deleteBtnName = 'deleteName';
		var _toggleBtnID = 'toggleTopList';
		var _toggleBtnValue = 'Lock list'; // check it is locked or not
		var _instructionMessage = EMPTY_HACKLIST_BOX_INSTRUCTION_MESSAGE_HTML + EMPTY_HACKLIST_BOX_TOPLIST_EXTRA_INSTRUCTION_MESSAGE_HTML;
	} else
	if (listPlace == 'secondList') {
		// these differ between the lists
		var _addToListID = 'addToList2Btn';
		var _clearListID = 'clearList2Btn';
		var _wrapperDivID = 'hackHelperNameListDiv2';
		var _deleteBtnName = 'deleteName2';
		var _toggleBtnID = 'toggleSecondList';
		var _instructionMessage = EMPTY_HACKLIST_BOX_INSTRUCTION_MESSAGE_HTML;
	} else {
		// unknown caller - log an error
		GM_log('Error running writeNameList(). Unable to determine the unique ids for the param listPlace (' + listPlace + ')');
		return;
	}
	// build up our new markup
	var toggleLockButton = '<input type="button" id="' + _toggleBtnID + '" value="Lock list"/>';
	var addNameToListButton = '<input type="button" id="' + _addToListID + '" value="Add a name"/>';
	var deleteButton = '<input type="button" id="' + _clearListID + '" value="Clear list"/>';
	var namesHtml = getNamesHtml(listPlace);
	if (namesHtml == '') namesHtml = _instructionMessage;
	var wrapperDiv = document.getElementById(_wrapperDivID);
	wrapperDiv.innerHTML = toggleLockButton + addNameToListButton + deleteButton + '' + namesHtml;

	// set the locked/unlocked state of the list buttons
	if (getSavedData(listPlace).locked == true) {
		lockButtonsAndList(listPlace);
	} else {
		unlockButtonsAndList(listPlace);
	}

	// add in the listeners for the 'Lock list', 'Add a name' and 'Clear list' buttons
	document.getElementById(_toggleBtnID).addEventListener('click', toggleLockList, true);
	document.getElementById(_addToListID).addEventListener('click', addName, true);
	document.getElementById(_clearListID).addEventListener('click', clearAList, true);

	// add in the delete actions
	var buttonEls = document.getElementsByName(_deleteBtnName);
	for (var i=0; i<buttonEls.length; i++) {
		buttonEls[i].addEventListener('click', deleteName, true);
	}
};

// prompt the user if they really want to clear out the corresponding list - and clear it out if confirmed
function clearAList(e) {
	if (e.target.id == 'clearList1Btn') {
		var listPlace = 'topList';
		var messageStr = 'top';
	} else
	if (e.target.id == 'clearList2Btn') {
		var listPlace = 'secondList';
		var messageStr = 'second';
	} else {
		// unknown caller - log an error
		GM_log('Error running clearAList(). Unable to determine which list to clear');
		return;
	}
	if (confirm("Are you sure you want to clear out the " + messageStr + " list?")) {
		saveList(listPlace, '');
		writeNameList(listPlace);
	}
};

// add the name that is in the current hack lookup text area to the list asociated with the "Add a name" button
function addName(e) {
	if (e.target.id == 'addToList1Btn') {
		var listPlace = 'topList';
	} else
	if (e.target.id == 'addToList2Btn') {
		var listPlace = 'secondList';
	} else {
		// unknown caller - log an error
		GM_log('Error running addName(). Unable to determine the unique ids from the button');
		return;
	}
	var name = document.getElementsByName("lookup_name")[0].value;
	// read in our cookie data for the second hack list
	var nameStr = getSavedData(listPlace).list;
	if (nameStr == null) {
		nameStr = name;
	} else {
		nameStr += "~" + name;
	}
	nameStr = cleanNameStr(nameStr);
	saveList(listPlace, nameStr);
	writeNameList(listPlace);
};

// delete the requested pilot from the list of pilots to be hacked
function deleteName(e) {
	if (e.target.name == 'deleteName') {
		var listPlace = 'topList';
		var listWrapperID = 'hackHelperNameListDiv';
	} else
	if (e.target.name == 'deleteName2') {
		var listPlace = 'secondList';
		var listWrapperID = 'hackHelperNameListDiv2';
	} else {
		// unknown caller - log an error
		GM_log('Error running deleteName(). Unable to determine the unique ids from the button');
		return;
	}
	
	if (document.getElementById(listWrapperID).className.indexOf('locked') > -1) {
		return; // we are in locked mode so leave this node alone
	}
	
	var deleteName = '~' + e.target.id + '~';
	var nameStr = '~' + getSavedData(listPlace).list + '~';
	var newNameStr = nameStr.substr(0, nameStr.indexOf(deleteName)) + '~'
	newNameStr += nameStr.substr(nameStr.indexOf(deleteName) + deleteName.length);
	newNameStr = cleanNameStr(newNameStr);
	saveList(listPlace, newNameStr);
	writeNameList(listPlace);
};

// return the HTML for the list of names corresponding to the list being built
function getNamesHtml(listPlace) {
	if (listPlace == 'topList') {
		var _deleteBtnName = 'deleteName';
	} else
	if (listPlace == 'secondList') {
		var _deleteBtnName = 'deleteName2';
	} else {
		// unknown caller - log an error
		GM_log('Error running getNamesHtml(). Unable to determine the unique ids for the param listPlace (' + listPlace + ')');
		return;
	}
	
	// handle errors in the input string and clean them up
	var nameStr = getSavedData(listPlace).list;
	nameStr = cleanNameStr(nameStr);
	if (nameStr == null || nameStr == "") return "";
	var names = nameStr.split('~');
	var html = "";
	for (var i=0; i<names.length; i++) {
		if (names[i] == "") return;
		html += "<span class=\"nameToHackStyles\"><a href=\"javascript:document.name_lookup.lookup_name.value = '" + names[i] + "'; document.name_lookup.method.value = document.hacking.method.value; document.name_lookup.submit();\" style=\"cursor:pointer;\">" + names[i] + "</a> <a name=\"" + _deleteBtnName + "\" id=\"" + names[i] + "\" class=\"deleteNameStyles\">&times;</a></span>";
	}
	return html;
};


/*
*******************************************************************************************************************************
							Allow the Hack Data to be displayed elsewhere on the page
*******************************************************************************************************************************
*/

// when the hack data is on the page, clone it all and place it in a div to the right of the hack controls
function cloneTheHackData() {
	var tables = document.getElementsByTagName('TABLE');
	if (tables.length > 4) {
		// insert a wrapper to clone the hack tables into
		var hackWrapperDiv = document.createElement("div");
		hackWrapperDiv.setAttribute("id", "hackWrapper");
		tables[4].parentNode.insertBefore(hackWrapperDiv, tables[4]);
		// create an array to record the tables we want to clone
		var tablesToClone = [];
		var parentNode = hackWrapperDiv.parentNode;
		for (var i=0; i<parentNode.childNodes.length; i++) {
			var node = parentNode.childNodes[i];
			if (node.nodeType == 1 && node.tagName == 'TABLE') {
				tablesToClone[tablesToClone.length] = node;
			}
		}
		// get rid of the first table since it is our hack table
		tablesToClone.shift();
		// clone all the others into the wrapper and hide the real ones
		// we don't actually move them because if we do the infocenter 
		// scripts can't pick up the data they need
		for (var i=0; i<tablesToClone.length; i++) {
			hackWrapperDiv.appendChild(tablesToClone[i].cloneNode(true));
			tablesToClone[i].style.display = 'none';
		}
	}
};


/*
*******************************************************************************************************************************
							Shared Helper methods
*******************************************************************************************************************************
*/

// return our universe for use in keying our data store variable
function getUniverse() {
	return window.location.host.substr(0, window.location.host.indexOf('.'));
};


// deals with improperly formatted data string
function cleanNameStr(newNameStr) {
	if (newNameStr == null) return "";
	if (newNameStr.indexOf('~~') >= 0) {
		newNameStr = newNameStr.replace(/~~/g, '~');
	}
	if (newNameStr.indexOf('~') == 0) {
		newNameStr = newNameStr.substr(1);
	}
	if (newNameStr.lastIndexOf('~') == newNameStr.length - 1) {
		newNameStr = newNameStr.substr(0, newNameStr.length - 1);
	}
	return newNameStr;
};

// returns all related saved data for a list defined by the following keys:
//
// >> getSavedData('topList') or 	getSavedData('list1') returns the data for the top list
// >> getSavedData('secondList') or getSavedData('list2') returns the data for the second list
//
function getSavedData(listname) {
	switch (listname) {
		case 'topList' || 'list1': 		var key = 'HackHelperNames'; break;
		case 'secondList' || 'list2': 	var key = 'HackHelperNamesSecondList'; break;
	}
	var returnData = { 'list': '', 'locked': false }; // initially represent the default values
	if (key) {
		var prefx = getUniverse() + '-' + key;
		returnData.list = GM_getValue(prefx, ''); // default to empty list of pilot names
		returnData.locked = GM_getValue(prefx + '-locked', false); // default to unlocked button state
	}
	return returnData;
};

// sets the data for the lists using the same keys as getSavedData() above to determine which list
function saveList(listname, data) {
	switch (listname) {
		case 'topList' || 'list1': 		var key = 'HackHelperNames'; break;
		case 'secondList' || 'list2': 	var key = 'HackHelperNamesSecondList'; break;
	}
	if (key) {
		var prefx = getUniverse() + '-' + key;
		GM_setValue(prefx, data);
	}
};

function saveLock(listname, data) {
	switch (listname) {
		case 'topList' || 'list1': 		var key = 'HackHelperNames'; break;
		case 'secondList' || 'list2': 	var key = 'HackHelperNamesSecondList'; break;
	}
	if (key) {
		var prefx = getUniverse() + '-' + key + '-locked';
		GM_setValue(prefx, data);
	}
};


/*
*******************************************************************************************************************************
							Main Code Execution Section
*******************************************************************************************************************************
*/

// insert the hacking helper code containing the two lists to show below the "Hack" button
hackingHelperInit();
cloneTheHackData();
